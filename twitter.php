<?php
/* Start session and load library. */
session_start();
require_once('twitter/twitteroauth/twitteroauth.php');
require_once('twitter/config.php');

if(isset($_GET['logout']))
{
	session_start();
	session_destroy();
	 
	/* Redirect to page with the connect to Twitter option. */
	header('Location: /twitter.php'); exit();
}
if(isset($_GET['callback']))
{
	/* If the oauth_token is old redirect to the connect page. */
	if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
		$_SESSION['oauth_status'] = 'oldtoken';
		header('Location: /twitter.php?logout'); exit();
	}
	
	/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
	
	/* Request access tokens from twitter */
	$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
	
	/* Save the access tokens. Normally these would be saved in a database for future use. */
	$_SESSION['access_token'] = $access_token;
	
	/* Remove no longer needed request tokens */
	unset($_SESSION['oauth_token']);
	unset($_SESSION['oauth_token_secret']);
	
	/* If HTTP response is 200 continue otherwise send to connect page to retry */
	if (200 == $connection->http_code) {
		/* The user has been verified and the access tokens can be saved for future use */
		$_SESSION['status'] = 'verified';
		$_SESSION['twitter_login_status'] = 'OK';
		header('Location: /account/twitter_connect'); exit();
		echo "<pre>";
		print_r($connection->get('account/verify_credentials'));
		echo "</pre>";
		die();
	} else {
		/* Save HTTP status for error dialog on connnect page.*/
		header('Location: /twitter.php?logout'); exit();
	}
}

//redirect

/* Build TwitterOAuth object with client credentials. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
 
/* Get temporary credentials. */
$request_token = $connection->getRequestToken(OAUTH_CALLBACK);

/* Save temporary credentials to session. */
$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
 
/* If last connection failed don't display authorization link. */
switch ($connection->http_code) {
  case 200:
    /* Build authorize URL and redirect user to Twitter. */
    $url = $connection->getAuthorizeURL($token);
    header('Location: ' . $url); 
    break;
  default:
    /* Show notification if something went wrong. */
    echo 'Could not connect to Twitter. Refresh the page or try again later.';
}
exit();