<?php echo $this->load->view('subheader', $data, FALSE); ?>

<!--main start-->
<div id="main" class="clearfix">
<div class="container clearfix">


  
<h3 class="pagetitle">Terima Kasih</h3>

<div id="success" class="text-wrap clearfix">
<div class="full">
  <p class="single-text">Data kamu sudah tersimpan<br>
  dan akan melalui proses moderasi.<br>Harap tunggu 2 x 24 jam.</p>

  <div class="boxes">
    <div id="box1"><a href="<?php echo site_url('gallery');?>" class="to-galeri">Lihat Galeri</a> <p>Lihat Galeri</p></div>
    <?php echo $this->load->view('box_social', $data, FALSE); ?>
  </div>
</div>
</div>

<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->