<!--footer-->
<div id="footer" class="clearfix">
  <div class="container">
    <ul class="nav clearfix" id="btm-nav">
      <li><?php echo anchor('', 'Home', ''); ?></li>
      <li><?php echo anchor('gallery', 'Galeri', ''); ?></li>
      <li><?php echo anchor('howto', 'How To', ''); ?></li>
      <li><?php echo anchor('tnc', 'Syarat dan ketentuan', ''); ?></li>
      <li class="to-top"><a href="#header">&uarr; Back to top</a></li>
    </ul>
    <ul class="socmed">
      <li><a href="http://twitter.com/" class="tw">Twitter</a></li>
      <li><a href="http://facebook.com/" class="fb">Facebook</a></li>
    </ul><!--social-->
    
    <p id="copyright">&copy; 2014 USBI. All rights reserved.</p>
  </div>
</div><!--footer end-->

</div><!--container-->

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<script src="<?php echo base_url('_assets'); ?>/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<?php /* ?><script src="<?php echo base_url('_assets'); ?>/js/script.js" type="text/javascript"></script><?php */ ?>
</body></html>
