<?php  
$data['list_artikel'] = OArtikel::get_list($page,$perpage,"id DESC","flag_2 = 1");
$total_fase_2 = get_db_total_rows();
$data['list_artikel'] = OArtikel::get_list($page,$perpage,"id DESC","flag_3 = 1");
$total_fase_3 = get_db_total_rows();
?>

<div class="phases">
  <a href="<?php echo site_url('gallery') ?>" <?php if($fase == "fase-1") { ?> class="active" <?php } ?>  title="See Shortlisted">Shortlist</a>
  <?php  
  if($total_fase_2 > 0){
  ?>
  <a href="<?php echo site_url('gallery/fase2') ?>" <?php if($fase == "fase-2") { ?> class="active" <?php } ?> title="See Top 20">20 Besar</a>
  <?php  
  }
  if($total_fase_3 > 0){
  ?>
  <a href="<?php echo site_url('gallery/fase3') ?>" <?php if($fase == "fase-3") { ?> class="active" <?php } ?> title="Winner">Winner</a>
  <?php } ?>
</div>