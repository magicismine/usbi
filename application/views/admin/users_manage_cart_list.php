<h2>Cart Data</h2><br/>

<div class="pull-left"><?=anchor($curpage."{$curid}/add", "+ Create new",array("class" => "button positive"))?></div>
<div class="clearfix"></div>
<br />
<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?php
	if(!$list) echo "<p class='error'>The Carts is empty.</p>";
    else
    {
?>
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
					<th>No.</th>
                	<th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OCart();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$i?></td>
                    <td>
                    <?=anchor($curpage."{$curid}/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />")?>                    <?=anchor($curpage."{$curid}/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"))?></td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        

        <?=$pagination?>
<?php
  }
?>
