
<h2>Users Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?=$_GET["keyword"]?>" />
    </form>
</div>
<div class="pull-left"><?=anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success"))?></div>
<div class="clearfix"></div><br />

<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?php
	if(!$list) echo "<p class='error'>The Users is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
					<th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>FB ID</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OUser();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$i+$uri;?></td>
                    <td><?=$name?></td>
					<td><?=$email?></td>
					<td><?=$phone?></td>
					<td><?=$age?></td>
					<td><?=$address?></td>
					<td><?=$fb_id?></td>
					<td><?php $actions = NULL;$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');")); echo implode(" ", $actions);?></td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        
        <?=$pagination?>
<?php
  }
?>
