
<h2>Admins Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?=$_GET["keyword"]?>" />
    </form>
</div>
<div class="pull-left"><?=anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success"))?></div>
<div class="clearfix"></div><br />

<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?php
	if(!$list) echo "<p class='error'>The Admins is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
                                            <th>Dt Added</th>
						<?php
												 $http_query = array();
												 $http_query["orderby"] = $_GET["orderby"];
												 $http_query["order"] = "ASC";		 
												 
												 if($_GET["orderby"] == "username")
												 {
													 if($_GET["order"] == "ASC")
													 {
														$http_query["order"] = "DESC";		 
													 }
													 else
													 {
														 $http_query["order"] = "ASC";		 
													 }
												 }
												 $http_query["keyword"] = $_GET["keyword"];
												 $http_query["orderby"] = "username";
												 echo "<th>".anchor($this->curpage."?".http_build_query($http_query),"Username",array("style" => ""))."</th>";
												 ?><?php
												 $http_query = array();
												 $http_query["orderby"] = $_GET["orderby"];
												 $http_query["order"] = "ASC";		 
												 
												 if($_GET["orderby"] == "password")
												 {
													 if($_GET["order"] == "ASC")
													 {
														$http_query["order"] = "DESC";		 
													 }
													 else
													 {
														 $http_query["order"] = "ASC";		 
													 }
												 }
												 ?>	<th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OAdmin();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
                    <td><?=$dt_added?></td>
					<td><?=$username?></td>
					<td><?php 
					$actions = NULL;
					$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");
					if($id != 1)
					{
					$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
					}
					echo implode(" ", $actions);?></td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        
        <?=$pagination?>
<?php
  }
?>
