<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OArtikel();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Artikel Form</h2><br/>	
<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?=form_open()?>
	<table class="tbl_form">
		<tr>
            <th>Name</th>
            <td>
            	<input type="text" name="name" value="<?=$name?>" required  /> 
                <br/><?=form_error('name')?>                
            </td>
        </tr>
		<tr>
            <th>Url</th>
            <td>
            	<input type="text" name="url" value="<?=$url?>" required  /> 
                <br/><?=form_error('url')?>                
            </td>
        </tr>
		<tr>
            <th>Content</th>
            <td>
            	<textarea name="content" cols="80" rows="20"><?=$content?></textarea>
				<br/><?=form_error('content')?>                
            </td>
        </tr>
		<tr>
            <th>Lolos Tahap 1</th>
            <td>
            	<select name="flag_1">
                    <option <?php if($flag_1 == 0) { echo "selected='selected'"; }?> value="0">Tidak Lolos</option>
                    <option <?php if($flag_1 == 1) { echo "selected='selected'"; }?> value="1">Lolos</option>
                </select>
            </td>
        </tr>
        <tr>
            <th>Lolos Tahap 2</th>
            <td>
                <select name="flag_2">
                    <option <?php if($flag_2 == 0) { echo "selected='selected'"; }?> value="0">Tidak Lolos</option>
                    <option <?php if($flag_2 == 1) { echo "selected='selected'"; }?> value="1">Lolos</option>
                </select>
            </td>
        </tr>
        <tr>
            <th>Lolos Tahap 3</th>
            <td>
                <select name="flag_3">
                    <option <?php if($flag_3 == 0) { echo "selected='selected'"; }?> value="0">Tidak Lolos</option>
                    <option <?php if($flag_3 == 1) { echo "selected='selected'"; }?> value="1">Lolos</option>
                </select>
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/artikels")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>