
<h2>Artikels Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?=$_GET["keyword"]?>" />
    </form>
</div>
<div class="pull-left"><?=anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success"))?></div>

<div class="clearfix"></div><br />
<div style="display:block; margin-bottom:10px;">
<form action="" method="get">
    Sort by:
    <input type="hidden" name="keyword" value="<?=$_GET["keyword"]?>" />
    <select name="sort_by" onchange='this.form.submit()'>
        <option <?php if($_GET['sort_by'] == "newest"){ echo "selected";} ?> value="newest">Newest</option>
        <option <?php if($_GET['sort_by'] == "total_vote"){ echo "selected";} ?> value="total_vote">Vote</option>
    </select>
</form>
</div>

<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?php
	if(!$list) echo "<p class='error'>The Artikels is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>					<th>No.</th>
                                            <th>User's Name</th>
						                            <th>Name</th>
						                            <th>Url</th>
						                            <th>Lolos Tahap 1</th>
                                                    <th>Lolos Tahap 2</th>
                                                    <th>Lolos Tahap 3</th>
                                                    <th>Total Vote</th>
							<th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OArtikel();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
                    <td><?php echo $i; ?></td>
                    <td>
                        <?php  
                        $Tmp = new OUser($user_id);
                        echo $Tmp->row->name;
                        unset($Tmp);
                        ?>
                    </td>
					<td><?=trimmer($name,40)?></td>
					<td><?=anchor($url,trimmer($url,25),array('target' => '_blank'));?></td>
					<td align="center"><?=(($flag_1== 0 || $flag_1 == "") ?
                    anchor($this->curpage."/set_lolos_1/1/".$id."?".get_params($_GET), "<img src='".base_url("_assets/images/disabled.gif")."' alt='NO' />",array('title' => 'Click to enable')) :
                    anchor($this->curpage."/set_lolos_1/0/".$id."?".get_params($_GET), "<img src='".base_url("_assets/images/enabled.gif")."' alt='YES' />",array('title' => 'Click to disable')))?>
                    </td>
                    <td align="center">
                        <?=(($flag_2== 0 || $flag_2 == "") ?
                    anchor($this->curpage."/set_lolos_2/1/".$id."?".get_params($_GET), "<img src='".base_url("_assets/images/disabled.gif")."' alt='NO' />",array('title' => 'Click to enable')) :
                    anchor($this->curpage."/set_lolos_2/0/".$id."?".get_params($_GET), "<img src='".base_url("_assets/images/enabled.gif")."' alt='YES' />",array('title' => 'Click to disable')))?>

                    </td>
                    <td align="center">
                        <?=(($flag_3== 0 || $flag_3 == "") ?
                    anchor($this->curpage."/set_lolos_3/1/".$id."?".get_params($_GET), "<img src='".base_url("_assets/images/disabled.gif")."' alt='NO' />",array('title' => 'Click to enable')) :
                    anchor($this->curpage."/set_lolos_3/0/".$id."?".get_params($_GET), "<img src='".base_url("_assets/images/enabled.gif")."' alt='YES' />",array('title' => 'Click to disable')))?>
                    </td>
                    <td align="center">
                        <?php  
                            echo $total_vote." Vote <br>";
                        ?>
                        <?php echo anchor('admin/artikels/lihat_vote/'.$id,"Lihat") ?>
                    </td>
					<td>
                    <?php 
                    $actions = NULL;
                    $actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");
                    $actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
                    echo implode(" ", $actions);?>
                    </td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        
        <?=$pagination?>
<?php
  }
?>
