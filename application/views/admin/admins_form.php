<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OAdmin();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Admin Form</h2><br/>	
<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Username</th>
            <td>
            	<input type="text" name="username" value="<?=$username?>" required autofocus /> 
                <br/><?=form_error('username')?>                
            </td>
        </tr>
		<tr>
            <th>Password</th>
            <td>
            	<input type="text" name="password" value="" required  /> 
                <br/><?=form_error('password')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/admins")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>