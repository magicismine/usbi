<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OCart();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Cart Form</h2><br/>	
<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?=form_open()?>
	<table class="tbl_form">
    			<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/users/manage_carts/{$curid}")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>