<h2>Change Password</h2>
<?=($this->session->flashdata('warning') != "" ? "<p class='error'>".$this->session->flashdata('warning')."</p>" : "")?>
<p>To change the current password, please enter new password below:</p>
<form action="" method="post">
	<table width="300" cellpadding="5">
    	<tr>
        	<td>New Password</td>
            <td><input type="password" name="password" /></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td><button type="submit">Change Password</button></td>
        </tr>
    </table>
</form>
