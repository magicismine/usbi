<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OUser();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>User Form</h2><br/>	
<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Name</th>
            <td>
            	<input type="text" name="name" value="<?=$name?>" required autofocus /> 
                <br/><?=form_error('name')?>                
            </td>
        </tr>
		<tr>
            <th>Email</th>
            <td>
            	<input type="text" name="email" value="<?=$email?>" required  /> 
                <br/><?=form_error('email')?>                
            </td>
        </tr>
		<tr>
            <th>Phone</th>
            <td>
            	<input type="text" name="phone" value="<?=$phone?>" required  /> 
                <br/><?=form_error('phone')?>                
            </td>
        </tr>
		<tr>
            <th>Address</th>
            <td>
            	<textarea name="address" cols="80" rows="3" class="mceNoEditor"><?=$address?></textarea>
				<br/><?=form_error('address')?>                
            </td>
        </tr>
		<tr>
            <th>FB ID</th>
            <td>
            	<input type="text" name="fb_id" value="<?=$fb_id?>" required  /> 
                <br/><?=form_error('fb_id')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/users")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>