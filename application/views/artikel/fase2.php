<?php echo $this->load->view('subheader', $data, FALSE); ?>

<!--main start-->
<div id="main" class="clearfix next-phase">
<div class="container clearfix">

<?php echo $this->load->view('tpl_fase', array('fase' => $fase,'total' => $total), TRUE); ?>
  
<h3 class="pagetitle">20 Besar "USBI Young Future Leader Blogging Competition
'</h3>
  <?php if($fase == "fase-2"): ?>
  <p class="single-text"><span style="font-size:10px; ">Tulisan peserta yang berhasil melalui proses penjurian tahap pertama.</span></p>
  <?php else: ?>
  <p class="single-text"><span style="font-size:10px; ">Pemenang hasil penjurian tahap final &amp; pemenang favorit pilihan pengunjung.</span></p>
  <?php endif; ?>
  <?php echo $this->session->flashdata('error'); ?>
<ul id="post-list" class="clearfix">
  <?php   
  if($list_artikel):
    foreach($list_artikel as $artikel):
      $OA->setup($artikel);
      $img = (array) json_decode($artikel->images);
      $img_logo = $img[0];
      foreach($img as $val)
      {
        if(strpos($val,'logo') !== false)
        {
          $img_logo = $val;
          break;
        }
      }

      $OU = new OUser($artikel->user_id);
      $list_votes = $OA->get_votes();
      $total_votes = get_db_total_rows();

    ?>
  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="<?php echo $artikel->url; ?>" class="avatar" target="_blank" title="See article"><img src="<?php echo $img_logo;?>" alt="" width="103" height="103" style="max-width:103px; max-height:103px;"></a>
        <h4 class="title"><a target="_blank"  href="<?php echo $artikel->url; ?>"><?php echo $artikel->name; ?></a></h4>
        <i class="owner">Oleh: <em><?php echo $OU->row->name; ?></em></i>
        <p class="excerpt"><?php echo $artikel->content; ?>.</p>
        <a href="<?php echo $artikel->url; ?>"  target="_blank" class="goto" title="See article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
            <div class="summary"><img src="<?php echo base_url('_assets') ?>/img/heart.png" width="17" height="14" alt="vote summary"> <i><?php echo $total_votes; ?></i></div>
            <a href="<?php echo site_url('gallery/vote/'.$artikel->id); ?>" class="vote-btn" title="Vote this!">Vote</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  <?php  
    endforeach;
  else:
  ?>
  <p class="single-text"><span style="font-size:10px; ">Belum ada peserta di 20 besar ini.</span></p>
  <?php  
  endif;
  ?>
</ul>

<?php echo $pagination; ?>

<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->