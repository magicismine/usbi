<?php echo $this->load->view('subheader', $data, FALSE); ?>
<!--main start-->
<div id="main" class="clearfix">
<div class="container clearfix">


<?php echo $this->load->view('tpl_fase', array('fase' => $fase), TRUE); ?>

<h3 class="pagetitle">Daftar peserta USBI Young Future Leader</h3>
<ul id="post-list" class="clearfix">

  <p class="single-text"><span style="font-size:10px; ">Tulisan peserta yang ditampilkan telah lolos proses moderasi.</span></p>
  <?php   
  if($list_artikel):
    foreach($list_artikel as $artikel):
      $OA->setup($artikel);
      $img = (array) json_decode($artikel->images);
      $img_logo = $img[0];
      foreach($img as $val)
      {
        if(strpos($val,'logo') !== false)
        {
          $img_logo = $val;
          break;
        }
      }

      $OU = new OUser($artikel->user_id);
    ?>
    <li>
    	<div class="wrap clearfix">
        <a href="<?php echo $artikel->url; ?>"  target="_blank" class="avatar"><img src="<?php echo $img_logo;?>" alt="" width="103" height="103" style="max-width:103px; max-height:103px;"></a>
        <h4 class="title"><a target="_blank"  href="<?php echo $artikel->url; ?>"><?php echo $artikel->name; ?></a></h4>
        <i class="owner">Oleh: <em><?php echo $OU->row->name; ?></em></i>
        <a href="<?php echo $artikel->url; ?>"  target="_blank" class="goto" title="See Article">See Article</a>
      </div>
    </li>
    <?php  
    endforeach;
  else:
  ?>
  <p class="single-text"><span style="font-size:10px; ">Belum ada judul yang dikirim.</span></p>
  <?php  
  endif;
  ?>
</ul>
<?php echo $pagination; ?>

<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->