<?php echo $this->load->view('subheader', $data, FALSE); ?>
<!--main start-->
<div id="main" class="clearfix next-phase">
<div class="container clearfix">

<?php echo $this->load->view('tpl_fase', array('fase' => $fase,'total' => $total), TRUE); ?>
<h3 class="pagetitle">Pemenang "USBI Young Future Leader Blogging Competition"</h3>

  <p class="single-text"><span style="font-size:10px; ">Pemenang hasil penjurian tahap final &amp; pemenang favorit pilihan pengunjung.</span></p>


<ul id="post-list" class="clearfix">
  <?php   
  if($list_artikel):
    foreach($list_artikel as $artikel):
      $OA->setup($artikel);
      $img = (array) json_decode($artikel->images);
      $img_logo = $img[0];
      foreach($img as $val)
      {
        if(strpos($val,'logo') !== false)
        {
          $img_logo = $val;
          break;
        }
      }

      $OU = new OUser($artikel->user_id);
    ?>
  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="<?php echo $artikel->url; ?>"  target="_blank" class="avatar" title="See Article"><img src="<?php echo $img_logo;?>" alt="" width="103" height="103" style="max-width:103px; max-height:103px;"></a>
        <h4 class="title"><a target="_blank"  href="<?php echo $artikel->url; ?>"><?php echo $artikel->name; ?></a></h4>
        <i class="owner">Oleh: <em><?php echo $OU->row->name; ?></em></i>
        <p class="excerpt"><?php echo $artikel->content; ?></p>
        <a href="<?php echo $artikel->url; ?>"  target="_blank" class="goto" title="See Article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
           	<a target="_blank" href="http://www.facebook.com/share.php?u=<?php echo $artikel->url; ?>" class="btn1" title="Share to Facebook">Facebook</a>
            <a target="_blank" href="http://twitter.com/share?text=<?php echo trimmer($artikel->name,30); ?>&url=<?php echo $artikel->url; ?>" class="btn2" title="Share to Twitter">Twitter</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  <?php  
    endforeach;
  else:
  ?>
  <p class="single-text"><span style="font-size:10px; ">Belum ada juara yang lolos.</span></p>
  <?php  
  endif;
  ?>
</ul>


<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->