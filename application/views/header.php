<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie6"><![endif]-->
<!--[if IE 7 ]><html class="ie7"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html id="<?php echo ($nav == "home" ? 'home' : '')?>" lang="id">
<!--<![endif]-->
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url('_assets'); ?>/css/css.css" type="text/css" rel="stylesheet" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>USBI - Blog Competition</title>
</head>

<script type="text/javascript">
/*$(window).unload(function() {
    var answer=confirm("Are you sure you want to leave?");
  if(answer){
      //ajax call here
      alert("");
      }
  });*/
/*var triedToLeave = false;
function onBeforeUnload(){
    triedToLeave = true;
    return "you can put your own message here";
}
setInterval( function (){
     if( triedToLeave ){
         window.removeEventListener( "beforeunload", onBeforeUnload, false );
         document.location.href = "http://stackoverflow.com/";
     }
}, 2000 );
window.addEventListener( "beforeunload", onBeforeUnload, false);
*/
/*window.onbeforeunload = function() {
    return "Are you sure you want to leave this page bla bla bla?"; // you can make this dynamic, ofcourse...
 };*/
 /*var dont_confirm_leave = 0; //set dont_confirm_leave to 1 when you want the user to be able to leave withou confirmation
        var leave_message = 'You sure you want to leave?'
        function goodbye(e) 
        {
            if(dont_confirm_leave!==1)
            {
                if(!e) e = window.event;
                //e.cancelBubble is supported by IE - this will kill the bubbling process.
                e.cancelBubble = true;
                e.returnValue = leave_message;
                //e.stopPropagation works in Firefox.
                if (e.stopPropagation) 
                {
                    e.stopPropagation();
                    e.preventDefault();
                }

                //return works for Chrome and Safari
                return leave_message;
            }
        }   
    window.onbeforeunload=goodbye;*/
</script>
<script src="http://connect.facebook.net/en_US/all.js"></script> 
<script> 

</script> 

<body>
<div id="fb-root"></div>
<script>
FB.init({
  appId:'<?php echo FB_APP_ID; ?>',
  cookie:true,
  status:true,
  xfbml:true
});
 
function FacebookInviteFriends()
{
  FB.ui({
  method: 'apprequests',
  message: 'Invite your friends',
  });
}
</script>
<div id="container">

<!--header-->
<div id="header" class="clearfix">
	<div class="container">

    <div id="logo"><?php echo anchor("", '<img src="'.base_url('_assets').'/img/usbi-logo.jpg" alt="USBI - Universitas Siswa Bangsa Internasional">'); ?></div><!--logo-->

    <ul class="nav" id="top-nav">
      <li class="last"><?php echo anchor('', 'Home', ''); ?></li>
      <li><?php echo anchor('gallery', 'Galeri'); ?></li>
      <li><?php echo anchor('howto', 'How To'); ?></li>
      <li><?php echo anchor('tnc', 'Syarat dan ketentuan', ''); ?></li>
    </ul><!--nav-->

    <ul class="socmed">
      <li><a target="_blank" href="https://twitter.com/usbi_indonesia" class="tw">Tweet</a></li>
      <li><a target="_blank" href="https://www.facebook.com/usbi.indonesia" class="fb">Facebook</a></li>
    </ul><!--social-->

    <div id="to-nav"><a href="#footer">Nav</a></div><!--to-nav-->

  </div>
</div><!--header end-->