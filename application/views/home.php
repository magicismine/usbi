<!--main start-->
<div id="main" class="clearfix">
<div class="container">

<div id="appname">
  <h2 id="name1">Young Future Leader</h2>
  <div id="name2">- Blogging Competition -</div>
</div>

<div id="teaser">
	<div class="wrap">
  	
    <div id="teaser-text">
    	<div class="wrap">
    	<div id="teaser-context">
      	<div>
          <p>Young Future Leader bertujuan menginspirasi generasi muda menjadi pemimpin di masa depan.<br><br> Kini saatnya kamu menulis &ldquo;We are The Future Leaders&rdquo; versimu dan menangkan beasiswa dari USBI.</p>
</div>
			</div>
      
      <div id="cta-home">
    	<a href="<?php echo site_url('artikel') ?>" id="cta1">Masukkan Cerita</a>
    	<a href="<?php echo site_url('gallery') ?>"  id="cta2">Lihat Galeri</a>
    </div>
      </div>
    </div>
    
    
    
	  <img src="<?=base_url();?>_assets/img/mbp.png" id="mbp" alt=" ">

  </div>
</div>

</div>
</div><!--main end-->