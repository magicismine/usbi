<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tnc extends CI_Controller {

	public function index()
	{
		$this->load->view('header', $data, FALSE);		
		$this->load->view('syarat_ketentuan', $data, FALSE);		
		$this->load->view('footer', $data, FALSE);		
	}

}

/* End of file tnc.php */
/* Location: ./application/controllers/tnc.php */