<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artikels extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/";
        $this->curpage 	= "admin/artikels";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($start=0)
    {
        $start 				= intval($_GET['page']);
        $perpage 			= 10;
        $data['orderby'] = $_GET['orderby'];
        $data['order'] = $_GET['order'];


        $sort_by = $_GET['sort_by'];
        if(sizeof($sort_by) > 0 )
        {
            switch ($sort_by) 
            {
                case 'total_vote':
                    $orderby = "total_vote DESC";
                    break;
                case 'newest':
                    $orderby = "id DESC";
                    break;
                default:
                    $orderby = "id DESC";
                    break;
            }
        }

        if($data['orderby'] == "" && $data['order'] == "") $ordering = "";
        else
        {
        	$ordering = "{$data['orderby']} {$data['order']}";
        }
        if($_GET['keyword'] != "")
        {
        	$data['list'] 	= OArtikel::search($_GET['keyword'],$orderby);
        }
        else
        {
        	
        	$data['list'] 	= OArtikel::get_list($start, $perpage,$orderby);
        }
       // var_dump($sort_by);

        $data['uri'] 		= intval($start);
        $total 				= get_db_total_rows();
        $url 				= $this->curpage."/listing?".get_params($_GET, array("page"));
        
        $data['pagination'] = getPagination($total, $perpage, $url, 5);
        
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'artikels_list', $data);
        $this->load->view($this->ffoot, $data);
    }
    
    public function add()
    {
    	// setup validations
        if(sizeof($_POST) > 0)
        {
		extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    if(trim($val) != "") $arr[$key] = trim($val);	
                }
            }
            
            $new = OArtikel::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
                        redirect($this->curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'artikels_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function edit($id)
    {
        $O = new OArtikel($id);
        
        if(!empty($id))
        {			
            if($O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($this->curpage);
                exit();
            }
        }
        
        if(sizeof($_POST) > 0)
        {
		extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    $arr[$key] = trim($val);	
                }
            }
            
            $res = $O->edit($arr);
                        if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $data['row'] = $O->row;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'artikels_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function delete($id)
    {
        $O = new OArtikel($id);
        
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage);
        exit();
    }	
    
    // this is an ajax call from the view list for sorting purposes.
    public function sorting()
    {
            $sorts = explode(",",$this->input->post('sorts'));
            $this->db->update('artikels',array('ordering' => 0));
            $count = 1;
            foreach($sorts as $id)
            {
                    $this->db->update('artikels',array('ordering' => $count),array('id' => intval($id)));
                    $count++;
            }
            die("DONE");
    }

    public function set_lolos_1($new_status=0,$id){
        $page = $_GET['page'];
        $O = new OArtikel($id);
        //$O->edit(array('status' => "pasif"));

        $res = $O->edit(array('flag_1' => $new_status));
        if($res) warning("edit", "success");
        redirect($this->curpage."?".urldecode(get_params($_GET)));
        exit();
    }

    public function set_lolos_2($new_status=0,$id){
        $page = $_GET['page'];
        $O = new OArtikel($id);
        //$O->edit(array('status' => "pasif"));

        $res = $O->edit(array('flag_2' => $new_status));
        if($res) warning("edit", "success");
        redirect($this->curpage."?".urldecode(get_params($_GET)));
        exit();
    }

    public function set_lolos_3($new_status=0,$id){
        $page = $_GET['page'];
        $O = new OArtikel($id);
        //$O->edit(array('status' => "pasif"));

        $res = $O->edit(array('flag_3' => $new_status));
        if($res) warning("edit", "success");
        redirect($this->curpage."?".urldecode(get_params($_GET)));
        exit();
    }

    public function lihat_vote($artikel_id)
    {
        $page = ($_GET['page'] != "" ? $_GET['page'] : 0) ;
        $data['perpage'] = $perpage = 10;

        $OA = $data['OA'] = new OArtikel($artikel_id);
        $OA->get_votes();
        $total_votes = get_db_total_rows();
        $arr['total_vote'] = $total_votes;
        $OA->edit($arr);

        $list_vote = $data['list'] = $OA->get_votes($page,$perpage);    
        $data['total'] = $total = get_db_total_rows();

        $url = "admin/artikels/lihat_vote/".$artikel_id."/?".get_params($_GET, array("page"));
        $data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);

        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'votes_list', $data);
        $this->load->view($this->ffoot, $data); 
    }

    public function delete_vote($fb_id,$artikel_id)
    {
        $OA = $data['OA'] = new OArtikel($artikel_id);
        if($OA->id == ""){ redirect('admin/artikels/lihat_vote'); exit;}
        $OU = new OUser($fb_id,"fb_id");
        if($OU->id == ""){ redirect('admin/artikels/lihat_vote'); exit;}
        $res = $OU->delete_vote($artikel_id);

        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect('admin/artikels/lihat_vote/'.$artikel_id);
    }
    
        
        
}

/* End of file artikels.php */
/* Location: ./application/controllers/admin/artikels.php */