<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller 
{
	var $head = "admin/header";
	var $foot = "admin/footer";
	var $curpage = "admin/home";
	var $admin;
	
	public function __construct()
	{
		parent::__construct();
		$this->admin = get_current_admin();
	}
	
	public function index()
	{
		if(!$this->admin) $this->login();
		else $this->main();
	}
	
	public function main()
	{
		if(!$this->admin) admin_login();
		$data['home_nav'] = "active";
		
		$this->load->view($this->head, $data);
		$this->load->view('admin/home');
		$this->load->view($this->foot);
	}
	
	public function login()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
		$this->form_validation->set_error_delimiters('', '<br />');
		
		if ($this->form_validation->run() == TRUE)
		{
			extract($_POST);
			$admin_res = get_admin($username, md5($password));
			//var_dump($_POST, $username, md5($password)); die();
			if($admin_res == FALSE) $this->session->set_flashdata("warning", "Invalid account.");
			else set_login_session($username, md5($password));
			redirect($this->curpage); 
			exit();
		}
		$this->load->view('admin/header_login', $data);
		$this->load->view('admin/login_form');
		$this->load->view('admin/footer_login');
	}
	
	public function changepassword()
	{
		if(!$this->admin) $this->login();
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
		$this->form_validation->set_error_delimiters('', '<br />');
		
		if ($this->form_validation->run() == TRUE)
		{
			extract($_POST);
			$this->db->update('admins',array('password' => md5($password)),array('id' => $this->admin->id));
			$admin_res = get_admin($username, md5($password));
			//var_dump($_POST, $username, md5($password)); die();
			$this->session->set_flashdata("warning", "Password Updated.");
			set_login_session($this->admin->username, md5($password));
			redirect("admin/home/changepassword"); 
			exit();
		}
		$this->load->view($this->head, $data);
		$this->load->view('admin/change_password');
		$this->load->view($this->foot);
	}
	
	public function test($a = "image_upload")
	{
		if(!$this->admin) $this->login();
		if(sizeof($_POST) > 0)
		{
			foreach($_POST['image'] as $filename)
			{
				
				$files = auto_resize_photo($filename);
				echo "<p>Image <strong>{$filename}</strong> successfully uploaded and created:<br />";
				echo @implode("<br />",$files)."</p>";
			}
			die("UPLOAD PHOTOS DONE!!");
		}
		$this->load->view($this->head, $data);
		$this->load->view('admin/test_image_upload');
		$this->load->view($this->foot);
	}
	
	public function regenerate_thumbnails()
	{
		
		if ($handle = opendir($_SERVER['DOCUMENT_ROOT']."/_assets/images/temp/")) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != ".." && !stristr($entry,"resize_")) {
					echo "Resizing $entry ... ";
					auto_resize_photo($entry);
					echo "DONE<br />";
				}
			}
			closedir($handle);
		}
	}
	
	public function logout()
	{
		unset_login_session();
		redirect($this->curpage); 
		exit();
	}


	public function fbinvite()
	{
		$this->load->view('fb_invite');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/admin/home.php */