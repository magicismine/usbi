<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "header";
        $this->ffoot 	= "footer";
        $this->fpath 	= "artikel/";
        $this->curpage 	= "artikel/";
        //if cu fb_id != "" maka akan langsung ke submit
        $this->cu = $cu = get_logged_in_user();
        /*if(!$cu)
        {
        	user_login($this->uri->uri_string());
        	exit;
        }*/
        $this->cu_fb = $cu_fb = facebook_auth();// helpernya facebook return user_profile
        //var_dump($cu_fb);
    }

    public function index()
    {
    	$this->registrasi();
    }


    public function email_check($email, $id)
	{
		$check = $this->db->get_where('users', array('id <>' => $id, 'email' => $email), 1);
		if (!emptyres($check))
		{
			$this->form_validation->set_message('email_check', 'Silahkan menggunakan email lain.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

    public function registrasi()
    {
    	$cu = $this->cu;
    	$cu_fb = $data['cu_fb'] = $this->cu_fb;
    	$OU = new OUser($cu_fb['id'],"fb_id");
    	//var_dump($OU->row->id);die();
    	//$OU->setup($cu);
    	//var_dump($OU->id);
    	$data['has_artikel'] = $has_artikel = $OU->get_artikels();
    	//var_dump($this->db->last_query());
    	$this->form_validation->set_rules('url', 'URL', 'trim|required');

    	if(!$has_artikel)
    	{
	    	$this->form_validation->set_rules('name', 'Nama', 'trim|required');
	    	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check['.$OU->id.']');
	    	$this->form_validation->set_rules('address', 'Alamat', 'trim|required');
	    	$this->form_validation->set_rules('age', 'Usia', 'trim|required|numeric');
	    	$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
    	}

    	$this->form_validation->set_message('required', 'Maaf, mohon masukkan %s kamu dengan lengkap.');
    	$this->form_validation->set_error_delimiters('', '<br />');

    	if($this->form_validation->run() == TRUE)
    	{
    		// UPDATE users
			extract($_POST);
			//var_dump($_POST);die();
			if(!$has_artikel)
    		{
			$arr = array(
						"name" => $name, 
						"email" => $email, 
						"age" => $age, 
						"address" => $address,
						"phone" => $phone,
						"fb_id" => $cu_fb['id']
						);
			//$OU->edit($arr);
			$new_user = OUser::add($arr);
			}

			//var_dump($new_user);die();
    		// INSERT artikel
    		$html = file_get_contents_curl($url);
			//parsing begins here:
			$doc = new DOMDocument();
			@$doc->loadHTML($html);
			$nodes = $doc->getElementsByTagName('title');
			$dom->preserveWhiteSpace = false;

			//get and display what you need:
			$title = $nodes->item(0)->nodeValue;

			$metas = $doc->getElementsByTagName('meta');

			for ($i = 0; $i < $metas->length; $i++)
			{
			    $meta = $metas->item($i);
			    if($meta->getAttribute('name') == 'description')
			        $description = $meta->getAttribute('content');
			    if($meta->getAttribute('name') == 'keywords')
			        $keywords = $meta->getAttribute('content');
			}

			$images = $doc->getElementsByTagName('img');
			foreach ($images as $image)
			{
				$img_src[] = $image->getAttribute('src');
			}


			$arr2 = array(
						'name' => $title,
						'content' => $description,
						'images' => json_encode($img_src),
						'url' => $url
						);
			if(!$has_artikel)
    		{
    			$arr2['user_id'] = $new_user;
    		}else
    		{
    			$arr2['user_id'] = $OU->id;
    		}
    		
			$new_id = OArtikel::add($arr2);

			redirect('success');
			exit;

			/*echo "Title: $title". '<br/><br/>';
			echo "Description: $description". '<br/><br/>';
			echo "Keywords: $keywords";*/

    	}

    	$data['nav'] = "artikel";
		$this->load->view('header', $data);
		$this->load->view($this->fpath."/registrasi", $data);
		$this->load->view('footer', $data);	
    }

	public function detail()
	{
		$data['nav'] = "artikel";
		$this->load->view('header', $data);
		$this->load->view($this->fpath."/detail", $data);
		$this->load->view('footer', $data);
	}

		

	
	

}

/* End of file artikel.php */
/* Location: ./application/controllers/artikel.php */