<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	public function facebook_connect()
	{
		session_start();
		$rd = $_GET['rd'];
		/*if($_SESSION['facebook_login_status'] != "OK")
		{
			//var_dump($_SESSION['facebook_login_status']); die();
			session_destroy();
			header('Location: /'.FPATH.'facebook.php?rd='.$rd);
			//user_logout();
			exit;
		}*/
		
		require './facebook/src/facebook.php';
		
		// Create our Application instance (replace this with your appId and secret).
		$config = array();
		$config['appId'] = FB_APP_ID;
		$config['secret'] = FB_APP_SECRET;
		$config['fileUpload'] = false; // optional
		$facebook = new Facebook($config);
		
		// Get User ID
		$user = $facebook->getUser();
		if ($user) {
			try {
				// Proceed knowing you have a logged in user who's authenticated.
				$user_profile = $facebook->api('/me');
			} catch (FacebookApiException $e) {
				error_log($e);
				$user = null;
			}
		}

		if ($user) {
			$params = array( 'next' => APP_SITE_URL.'logout' );
			$logoutUrl = $facebook->getLogoutUrl($params); // $params is optional. 
		} else {
			$params = array(
							'scope' => 'email,user_likes,publish_stream',
							'redirect_uri' => site_url($rd)
							);
			$loginUrl = $facebook->getLoginUrl($params);
		}



		if(!$user_profile)
		{
			session_destroy();
			//var_dump($user_profile); die('LOGIN FIRST 2!');
			header('Location: /'.FPATH.'facebook.php?rd='.$rd);
			exit;
		}
		else {
			extract($user_profile);
			$res = $this->db->query("SELECT * FROM users WHERE fb_id = ? OR email = ?", array($user,$email));
			if(emptyres($res))
			{
				$this->load->helper(array("string"));
				
				$password = random_string("alnum", 20);
				$password = md5(md5($password));
				if(empty($name)) $name = $first_name." ".$last_name;
				
				$arr["fb_id"] = $user;
				$arr["email"] = $email;
				$arr["password"] = $password;
				$arr["name"] = $name;
				//if(!empty($gender)) $arr["gender"] = $gender;
				$new = OUser::add($arr);
				
				if($new)
				{
					$O = new OUser($new);
					
					$this->load->helper(array("image","string"));
					$this->config->load('image');
					$imagesizes = $this->config->item('image_sizes');
					
					$uploaddir = FCPATH.'/_assets/images/temp/';
					if(!is_dir($uploaddir)) mkdir($uploaddir,0777,true);
					$random = time()."_".random_string("alnum", 20);
					$newfile =  $random. ".jpg";
					$file = $uploaddir . $newfile;
					if (copy("http://graph.facebook.com/".$user."/picture?type=large", $file))
					{
						// resize the image to lower size
						$resize_newfile = "resize_".$newfile;
						resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
						$O->update_photo($newfile);
					}
					
					set_login_session($email,$password,"user");
				}
			}
			else {
				$row = $res->row();
				$fb_login_count = $row->fb_login_count + 1;
				//$arr["fb_login_count"] = $fb_login_count;
				$arr["fb_id"] = $user;
				//$arr["email"] = $email;
				$O = new OUser();
				$O->setup($row);
				$res = $O->edit($arr);
				if($res && empty($row->photo))
				{
					$this->load->helper(array("image","string"));
					$this->config->load('image');
					$imagesizes = $this->config->item('image_sizes');
					
					$uploaddir = FCPATH.'/_assets/images/temp/';
					if(!is_dir($uploaddir)) mkdir($uploaddir,0777,true);
					$random = time()."_".random_string("alnum", 20);
					$newfile =  $random. ".jpg";
					$file = $uploaddir . $newfile;
					if (copy("http://graph.facebook.com/".$user."/picture?type=large", $file))
					{
						// resize the image to lower size
						$resize_newfile = "resize_".$newfile;
						resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
						$O->update_photo($newfile);
					}
				}
				extract(get_object_vars($O->row));
				set_login_session($email,$password,"user");
			}

			if($_GET['rd'] != "") redirect($_GET['rd']);
			else redirect('artikel');
			exit();
		}
	}
	
	public function twitter_connect()
	{
		session_start();
		if($_SESSION['twitter_login_status'] != "OK")
		{
			session_destroy();
			user_logout();
			exit();
		}
		
		require_once('./twitter/twitteroauth/twitteroauth.php');
		
		/* If access tokens are not available redirect to connect page. */
		if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
			header('Location: /twitter.php?logout');
		}
		/* Get user access tokens out of the session. */
		$access_token = $_SESSION['access_token'];
		
		/* Create a TwitterOauth object with consumer/user tokens. */
		$connection = new TwitterOAuth(TW_CONSUMER_KEY, TW_CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
		
		/* If method is set change API call made. Test is called by default. */
		$user_profile = $connection->get('account/verify_credentials');

		if(!$user_profile)
		{
			session_destroy();
			user_logout();
			exit();
		}
		else {
			extract(get_object_vars($user_profile));
			$email = $screen_name;
			if(empty($id) || empty($email))
			{
				$this->session->set_flashdata("warning", "Sorry you can not Sign in with Twitter. Please try again later.");
				redirect($this->curpage); exit;
			}
			$res = $this->db->query("SELECT * FROM users WHERE twitter_id = ? OR email = ?", array($id,$email));
			if(emptyres($res))
			{
				$this->load->helper(array("string"));
				
				$password = random_string("alnum", 20);
				$password = md5(md5($password));
				
				$arr["twitter_id"] = $id;
				$arr["email"] = $email;
				$arr["password"] = $password;
				$arr["name"] = $name;
				if(!empty($gender)) $arr["gender"] = $gender;
				$new = OUser::add($arr);
				
				if($new)
				{
					$O = new OUser($new);
					
					$this->load->helper(array("image","string"));
					$this->config->load('image');
					$imagesizes = $this->config->item('image_sizes');
					
					$uploaddir = FCPATH.'/_assets/images/temp/';
					if(!is_dir($uploaddir)) mkdir($uploaddir,0777,true);
					$random = time()."_".random_string("alnum", 20);
					$newfile =  $random. ".jpg";
					$file = $uploaddir . $newfile;
					if (copy(str_replace("_normal", "", $profile_image_url), $file))
					{
						// resize the image to lower size
						$resize_newfile = "resize_".$newfile;
						resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
						$O->update_photo($newfile);
					}
					set_login_session($email,$password,"user");
				}
			}
			else {
				$row = $res->row();
				$twitter_login_count = $row->twitter_login_count + 1;
				$arr["twitter_login_count"] = $twitter_login_count;
				$arr["twitter_id"] = $id;
				//$arr["email"] = $email;
				$O = new OUser();
				$O->setup($row);
				$res = $O->edit($arr);
				if($res && empty($row->photo))
				{
					$this->load->helper(array("image","string"));
					$this->config->load('image');
					$imagesizes = $this->config->item('image_sizes');
					
					$uploaddir = FCPATH.'/_assets/images/temp/';
					if(!is_dir($uploaddir)) mkdir($uploaddir,0777,true);
					$random = time()."_".random_string("alnum", 20);
					$newfile =  $random. ".jpg";
					$file = $uploaddir . $newfile;
					if (copy(str_replace("_normal", "", $profile_image_url), $file))
					{
						// resize the image to lower size
						$resize_newfile = "resize_".$newfile;
						resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
						$O->update_photo($newfile);
					}
				}
				extract(get_object_vars($O->row));
				set_login_session($email,$password,"user");
			}
			if($_GET['rd'] != "") redirect($_GET['rd']);
			else redirect('artikel');
			exit();
		}
	}

}

/* End of file account.php */
/* Location: ./application/controllers/account.php */