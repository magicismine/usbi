<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		session_start();
		session_destroy();
		unset_login_session("user");
		redirect('');
	}
	
}

/* End of file logout.php */
/* Location: ./application/controllers/logout.php */