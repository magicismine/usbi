<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Syarat extends CI_Controller {

	public function index()
	{
		$this->load->view('header', $data, FALSE);		
		$this->load->view('syarat_ketentuan', $data, FALSE);		
		$this->load->view('footer', $data, FALSE);		
	}

}

/* End of file syarat.php */
/* Location: ./application/controllers/syarat.php */