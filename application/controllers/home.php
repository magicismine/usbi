<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "header";
        $this->ffoot 	= "footer";
        $this->fpath 	= "home/";
        $this->curpage 	= "home/";
       
    }

	public function index()
	{
		$data['nav'] = "home";
		$this->load->view('header', $data);
		$this->load->view('home', $data);
		$this->load->view('footer', $data);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */