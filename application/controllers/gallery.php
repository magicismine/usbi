<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "header";
        $this->ffoot 	= "footer";
        $this->fpath 	= "artikel/";
        $this->curpage 	= "gallery/";
       	$this->cu = $cu = get_logged_in_user();

    }

	public function index()
	{
		$this->shortlist();		
	}

	public function shortlist()
	{
		$data['OA'] = new OArtikel;
		$page = ($_GET['page'] != "" ? $_GET['page'] : 0) ;
		$data['perpage'] = $perpage = 10;
		$data['fase'] = "fase-1";

		$data['list_artikel'] = OArtikel::get_list($page,$perpage,"id DESC","flag_1 = 1");
		//$data['list_artikel'] = OArtikel::get_list($page,$perpage,"id DESC");
		$data['total'] = $total = get_db_total_rows();

		$url = $this->curpage."/shortlist?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);

		$data['nav'] = "artikel";
		$this->load->view('header', $data);
		$this->load->view($this->fpath."/listing", $data);
		$this->load->view('footer', $data);
	}

	public function fase2()
	{
		//var_dump($_SERVER['HTTP_REFERER']);die();
		//Fase 2, 20 Besar
		$data['fase'] = "fase-2";
		$data['OA'] = new OArtikel;
		$page = ($_GET['page'] != "" ? $_GET['page'] : 0) ;
		$data['perpage'] = $perpage = 20;

		$data['list_artikel'] = OArtikel::get_list($page,$perpage,"id DESC","flag_2 = 1");
		$data['total'] = $data['total_fase_2'] = $total = get_db_total_rows();

		if($total == 0){ redirect($_SERVER['HTTP_REFERER']);}

		$url = $this->curpage."/shortlist?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);

		$data['nav'] = "artikel";
		$this->load->view('header', $data);
		$this->load->view($this->fpath."/fase2", $data);
		$this->load->view('footer', $data);
	}

	public function fase3()
	{
		$data['fase'] = "fase-3";
		//Fase 3, winner Besar
		$data['OA'] = new OArtikel;
		$page = ($_GET['page'] != "" ? $_GET['page'] : 0) ;
		$data['perpage'] = $perpage = 10;

		$data['list_artikel'] = OArtikel::get_list($page,$perpage,"id DESC","flag_3 = 1");
		$data['total'] = $data['total_fase_3'] = $total = get_db_total_rows();

		if($total == 0){ redirect($_SERVER['HTTP_REFERER']);}

		$url = $this->curpage."/shortlist?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);

		$data['nav'] = "artikel";
		$this->load->view('header', $data);
		$this->load->view($this->fpath."/fase3", $data);
		$this->load->view('footer', $data);
	}

	public function vote($artikel_id)
	{
		$this->cu_fb = $cu_fb = facebook_auth();// helpernya facebook return user_profile

		$OU = new OUser($cu_fb['id'],"fb_id");
		//var_dump($OU->row->fb_id);die();
		if($cu_fb['id'] == "")
		{
			$this->session->set_flashdata('error', '<p class="error">Anda harus login facebook terlebih dahulu.</p>');
			redirect('gallery/fase2');
		}

		if($artikel_id == "")
		{
			$this->session->set_flashdata('error', '<p class="error">Anda belum memilih artikel yang akan di vote.</p>');
			redirect('gallery/fase2');
		}

		$check_vote = $OU->check_vote($artikel_id);
		if($check_vote == TRUE)
		{
			$this->session->set_flashdata('error', '<p class="error">Maaf, anda sudah vote artikel tersebut.</p>');
			redirect('gallery/fase2');
			exit;
		}else
		{
			$set_vote = $OU->set_vote($artikel_id);
			//var_dump($set_vote);die();
			$OA = new OArtikel($artikel_id);
			$OA->get_votes();
			$total_votes = get_db_total_rows();
			$arr['total_vote'] = $total_votes;
			$OA->edit($arr);

			redirect('gallery/success_vote');
			exit;
		}
		
	}

	public function success_vote()
	{
		$this->load->view('header', $data, FALSE);
		$this->load->view('success_vote', $data, FALSE);
		$this->load->view('footer', $data, FALSE);
	}


}

/* End of file gallery.php */
/* Location: ./application/controllers/gallery.php */