<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function index()
	{
		
	}
	
	function upload($a = "")
	{
		if($a == "image")
		{
			$this->load->helper(array("image","string"));
			$this->config->load('image');
			$imagesizes = $this->config->item('image_sizes');
			
			$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/_assets/images/temp/';
			$filename = basename($_FILES['uploadfile']['name']);
			$filearr = explode(".", $filename);
			$random = time()."_".random_string("alnum", 20);
			$newfile =  $random. "." . $filearr[count($filearr)-1];
			$file = $uploaddir . $newfile;
			$size = $_FILES['uploadfile']['size'];
			$tmps = array();
			if($size>1048576)
			{
				//echo "error file size > 1 MB";
				echo 'error';
				if(file_exists($_FILES['uploadfile']['tmp_name'])) unlink($_FILES['uploadfile']['tmp_name']);
				exit;
			}
			if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
			{
				// resize the image to lower size
				$resize_newfile = "resize_".$newfile;
				resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
				echo $newfile; 
			} else {
				//die('error');
				//echo "error ".$_FILES['uploadfile']['error']." --- ".$_FILES['uploadfile']['tmp_name']." %%% ".$file."($size)";
			}
		}
		
		
		if($a == "all_files")
		{
			//$this->load->helper(array("image","string"));
			$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/_assets/all_downloads/temp/';
			$filename = basename($_FILES['uploadfile']['name']);
			$filearr = explode(".", $filename);
			/*$newfile = time()."_".random_string("alnum", 20) . "." . $filearr[count($filearr)-1];*/
			
			$newfile = $filename;
			$file = $uploaddir . $newfile;
			$size = $_FILES['uploadfile']['size'];
			$tmps = array();
			if($size>1048576)
			{
				//echo "error file size > 1 MB";
				echo 'error';
				if(file_exists($_FILES['uploadfile']['tmp_name'])) unlink($_FILES['uploadfile']['tmp_name']);
				exit;
			}
			if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
			{
				// resize the image to lower size
				//$resize_newfile = "resize_".$newfile;
				//resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
				//crop_photo($file,$uploaddir.$resize_newfile,110,120);
				echo $newfile; 
			} else {
				//die('error');
				//echo "error ".$_FILES['uploadfile']['error']." --- ".$_FILES['uploadfile']['tmp_name']." %%% ".$file."($size)";
			}
		}

	}
	
}