<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Success extends CI_Controller {

	public function index()
	{
		$this->thank_you();
	}

	public function thank_you()
	{
		$data['nav'] = "artikel";
		$this->load->view('header', $data);
		$this->load->view("success", $data);
		$this->load->view('footer', $data);
	}

}

/* End of file success.php */
/* Location: ./application/controllers/success.php */