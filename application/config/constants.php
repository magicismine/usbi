<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

if(stristr($_SERVER['HTTP_HOST'],"usbi.dev"))
{
	define('FPATH', '');
	define('FB_APP_ID', '1415287262051581');
	define('FB_APP_SECRET', 'd36232bd08cdf2fdf28940fc0534474e');
}
elseif(stristr($_SERVER['HTTP_HOST'],"localhost"))
{
	define('FPATH', 'usbi/');
}
else
{
	define('FPATH', 'usbi/');
}

define('FB_APP_ID', '639155686122342');
define('FB_APP_SECRET', '462463a1b5201261dc8fad061713ea1b');

define('TW_CONSUMER_KEY', 'LkR6OyesrQyuoAEpQpX9w');
define('TW_CONSUMER_SECRET', 'FXFSzz8dLZ5nNLe6XEOfmE94yR8W9AF8i56x2E5t47c');

/* End of file constants.php */
/* Location: ./application/config/constants.php */