<?php
/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
error_reporting(0);
require 'facebook/src/facebook.php';
require 'facebook/config.php';

// Create our Application instance (replace this with your appId and secret).
$config = array();
$config['appId'] = FB_APP_ID;
$config['secret'] = FB_APP_SECRET;
$config['fileUpload'] = false; // optional
$facebook = new Facebook($config);

// Get User ID
$user = $facebook->getUser();

// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

if ($user) {
  try {
    // Proceed knowing you have a logged in user who's authenticated.
    $user_profile = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }
}

// Login or logout url will be needed depending on current user state.
if ($user) {
	$params = array( 'next' => APP_SITE_URL.'logout' );
	$logoutUrl = $facebook->getLogoutUrl($params); // $params is optional. 
} else {
	$params = array(
					'scope' => 'email,user_likes,publish_stream',
					'redirect_uri' => APP_SITE_URL.'facebook.php?rd='.$_GET['rd']
					);
	$loginUrl = $facebook->getLoginUrl($params);
}

if(empty($user_profile))
{
    //var_dump($user); die;
	header("Location: $loginUrl");
} else {
	/*session_start();
	$_SESSION['facebook_login_status'] = "OK";
	header("Location: ".APP_SITE_URL."account/facebook_connect?rd=".$_GET['rd']);*/
	?>
    <?php ///*?>
	<h3>PHP Session</h3>
    <pre><?php print_r($_SESSION); ?></pre>
    
    <?php if ($user): ?>
    <h3>You</h3>
    <img src="https://graph.facebook.com/<?php echo $user; ?>/picture">
    <p><a href="<?=$logoutUrl?>">Logout</a></p>
    <h3>Your User Object (/me)</h3>
    <pre><?php print_r($user_profile); ?></pre>
    <pre><?php print_r($user_profile['email']); ?></pre>
    <?php else: ?>
    <strong><em>You are not Connected.</em></strong>
    <?php endif ?>
	<?php //*/?>
    <?
}
?>
