<?php include('include/header.php'); ?>

<div id="sub-header">
<div class="container">
<div id="cta-top"><a href="submit.php">Submit Cerita</a></div>
<div id="appname">
  <h2 id="name1">Young Future Leader</h2>
  <div id="name2">Blogging Competition</div>
</div>
</div>
</div><!--sub-header-->

<!--main start-->
<div id="main" class="clearfix next-phase">
<div class="container clearfix">

<div class="phases">
	<a href="galeri.php" title="See Shortlisted">Shortlist</a>
  <a href="galeri-fase2.php" class="active" title="See Top 20">20 Besar</a>
</div>
  
<h3 class="pagetitle">20 Besar "USBI Young Future Leader Blogging Competition
'</h3>

  <p class="single-text"><span style="font-size:10px; ">Tulisan peserta yang berhasil melalui proses penjurian tahap pertama.</span></p>

<ul id="post-list" class="clearfix">

  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="#" class="avatar" title="See article"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
        <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia</a></h4>
        <i class="owner">Oleh: <em>Paulo Coelho</em></i>
        <p class="excerpt">Masa depan Indonesia ada ditangan pemuda Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
        <a href="#" class="goto" title="See article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
            <div class="summary"><img src="img/heart.png" width="17" height="14" alt="vote summary"> <i>115</i></div>
            <a href="#" class="vote-btn" title="Vote this!">Vote</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="#" class="avatar" title="See article"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
        <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
        <i class="owner">Oleh: <em>Paulo Coelho</em></i>
        <p class="excerpt">Masa depan Indonesia ada ditangan pemuda Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
        <a href="#" class="goto" title="See Article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
            <div class="summary"><img src="img/heart.png" width="17" height="14" alt="vote summary"> <i>115</i></div>
            <a href="#" class="vote-btn" title="Vote this!">Vote</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="#" class="avatar" title="See article"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
        <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia</a></h4>
        <i class="owner">Oleh: <em>Paulo Coelho</em></i>
        <p class="excerpt">Masa depan Indonesia ada ditangan pemuda Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
        <a href="#" class="goto" title="See article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
            <div class="summary"><img src="img/heart.png" width="17" height="14" alt="vote summary"> <i>115</i></div>
            <a href="#" class="vote-btn" title="Vote this!">Vote</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="#" class="avatar" title="See article"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
        <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
        <i class="owner">Oleh: <em>Paulo Coelho</em></i>
        <p class="excerpt">Masa depan Indonesia ada ditangan pemuda Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
        <a href="#" class="goto" title="See Article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
            <div class="summary"><img src="img/heart.png" width="17" height="14" alt="vote summary"> <i>115</i></div>
            <a href="#" class="vote-btn" title="Vote this!">Vote</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="#" class="avatar" title="See article"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
        <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia</a></h4>
        <i class="owner">Oleh: <em>Paulo Coelho</em></i>
        <p class="excerpt">Masa depan Indonesia ada ditangan pemuda Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
        <a href="#" class="goto" title="See article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
            <div class="summary"><img src="img/heart.png" width="17" height="14" alt="vote summary"> <i>115</i></div>
            <a href="#" class="vote-btn" title="Vote this!">Vote</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="#" class="avatar" title="See article"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
        <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
        <i class="owner">Oleh: <em>Paulo Coelho</em></i>
        <p class="excerpt">Masa depan Indonesia ada ditangan pemuda Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
        <a href="#" class="goto" title="See Article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
            <div class="summary"><img src="img/heart.png" width="17" height="14" alt="vote summary"> <i>115</i></div>
            <a href="#" class="vote-btn" title="Vote this!">Vote</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  
</ul>

<ul id="pagination">
	<li><a href="#">&laquo;</a></li>
  <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#">&raquo;</a></li>
</ul>

<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->

<?php include('include/footer.php'); ?>