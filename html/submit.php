<?php include('include/header.php'); ?>

<!--main start-->
<div id="main" class="clearfix">
<div class="container clearfix">

<h3 class="pagetitle">Kirim tulisan kreatifmu & menangkan beasiswa USBI</h3>


	<form id="reg-form" class="clearfix" action="success.php">

    <div class="full" id="submission">
      <div class="wrap">
        <label for="url" id="url-label">Masukkan URL artikelmu mengenai LEADERSHIP</label>
        <input type="text" class="input-text" name="url" id="url" placeholder="Contoh: http://blog.com/nama-artikel">
      </div>
    </div>
    
    <div class="wrap clearfix">

    <div id="task" class="clearfix error">
      <div class="wrap">Maaf, mohon masukkan alamat URL artikel</div>
    </div>

    <p class="clearfix" id="reg-btn-wrap">
      <input type="submit" name="register" id="reg-btn" value="Kirim URL Artikel" class="input-submit button">
    </p>
    <p id="agree-terms" class="clearfix">Dengan mengklik tombol diatas saya menyatakan setuju dengan syarat dan ketentuan</p>
  </div>

  </form>

<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->

<?php include('include/footer.php'); ?>