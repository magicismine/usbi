<?php include('include/header.php'); ?>

<div id="sub-header">
<div class="container">
<div id="cta-top"><a href="submit.php">Submit Cerita</a></div>
<div id="appname">
  <h2 id="name1">Young Future Leader</h2>
  <div id="name2">Blogging Competition</div>
</div>
</div>
</div><!--sub-header-->

<!--main start-->
<div id="main" class="clearfix next-phase">
<div class="container clearfix">

<div class="phases">
	<a href="galeri.php" title="See Shortlisted">Shortlist</a>
  <a href="galeri-fase2.php" title="See Top 20">20 Besar</a>
  <a href="galeri-fase3.php" class="active" title="See Winners">Winner</a>
</div>

<h3 class="pagetitle">Pemenang "USBI Young Future Leader Blogging Competition"</h3>

  <p class="single-text"><span style="font-size:10px; ">Pemenang hasil penjurian tahap final &amp; pemenang favorit pilihan pengunjung.</span></p>


<ul id="post-list" class="clearfix">

  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="#" class="avatar" title="See Article"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
        <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia</a></h4>
        <i class="owner">Oleh: <em>Paulo Coelho</em></i>
        <p class="excerpt">Masa depan Indonesia ada ditangan pemuda Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
        <a href="#" class="goto" title="See Article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
           	<a href="#" class="btn1" title="Share to Facebook">Facebook</a>
            <a href="#" class="btn2" title="Share to Twitter">Twitter</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
    	
      <div class="left">
        <a href="#" class="avatar" title="See Article"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
        <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia ada ditangan pemuda Masa depan Indonesia</a></h4>
        <i class="owner">Oleh: <em>Paulo Coelho</em></i>
        <p class="excerpt">Masa depan Indonesia ada ditangan pemuda Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
        <a href="#" class="goto" title="See Article">See Article</a>
      </div>
      
			<div class="right">
      	<div class="tool">
          <div class="wrap">
           	<a href="#" class="btn1" title="Share to Facebook">Facebook</a>
            <a href="#" class="btn2" title="Share to Twitter">Twitter</a>
          </div>
      	</div>
      </div>

    </div>
  </li>
  
</ul>


<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->

<?php include('include/footer.php'); ?>