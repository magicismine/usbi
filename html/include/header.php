<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie6"><![endif]-->
<!--[if IE 7 ]><html class="ie7"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="id">
<!--<![endif]-->
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href="css/css.css" type="text/css" rel="stylesheet" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>USBI - Blog Competition</title>
</head>
<body>

<div id="container">

<!--header-->
<div id="header" class="clearfix">
	<div class="container">

    <div id="logo"><a href="index.php"><img src="img/usbi-logo.jpg" alt="USBI - Universitas Siswa Bangsa Internasional"></a></div><!--logo-->

    <ul class="nav" id="top-nav">
      <li class="last"><a href="index.php">Home</a></li>
      <li><a href="galeri.php">Galeri</a></li>
      <li><a href="how-to.php">How To</a></li>
      <li><a href="tnc.php">Syarat dan ketentuan</a></li>
    </ul><!--nav-->

    <ul class="socmed">
      <li><a href="#" class="tw">Twitter</a></li>
      <li><a href="#" class="fb">Facebook</a></li>
    </ul><!--social-->

    <div id="to-nav"><a href="#footer">Nav</a></div><!--to-nav-->

  </div>
</div><!--header end-->