<div class="curtain">
	<div class="inner">
    <div class="page-name">
          <h2 class="modal-title">Mekanisme</h2>
        </div>
    
    <div class="scroll-area">
    
    <div class="modal-content">
    <h4 class="subtitle">MEKANISME A NIGHT OF INFINITE PLEASURE – MAGNUM INFINITY</h4>
    
    <ol>
      <li>Peserta wajib dan telah menjadi Follower pada twitter @MyMagnumID.</li>
      <li>Peserta memilih salah satu host party dalam #MagnumHost http://bit.ly/MagnumHost.</li>
      <li>Minta undangan kepada Host Party melalui @MyMagnumID, sampaikan kalau kalian layak menjadi tamu di acara A Night of Infinite Pleasure.</li>
      <li>Peserta wajib mengirimkan tweet ke @MyMagnumID dengan template tweet yang disesuaikan pada halaman web Magnum Infinity A Night of Infinite. Template tweet: Take me to "A night of Infinite Pleasure" party + [Host yang dipilih: @_VinoGBastian_] + [berikan alasan kreatifmu] + [#MagnumHost] + [@MyMagnumID].</li>
      <li>Template tweet ke @MyMagnumID harus digunakan diakhir atau tengah kalimat, tidak boleh berbentuk reply ke @MyMagnumID.</li>
      <li>Host Party yang kita tentukan akan memilih peserta mana yang layak diundang ke acara A Night Infinite Pleasure.</li>
      <li>Contoh kalimat kreatif: Take me to "A night of Infinite Pleasure" party @_VinoGBastian_! Gw akan pakai baju super unik berlapis cokelat! #MagnumHost @MyMagnumID.</li>
      <li>Tweet dengan alasan yang paling kreatif akan diundang oleh Host Party dan tim Magnum Indonesia untuk menjadi peserta dalam acara <strong>A Night Infinite Pleasure</strong>.</li>
      <li>Pemenang akan mendapatkan masing-masing 2 (dua) Invitation Party dari Magnum Indonesia.</li>
      <li>Pengumuman pemenang dilakukan setiap hari, selama promo A Night Infinite Pleasure berlangsung.</li>
    </ol>

    </div>
    
    </div>
	</div>
</div>