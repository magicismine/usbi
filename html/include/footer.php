<!--footer-->
<div id="footer" class="clearfix">
  <div class="container">
    <ul class="nav clearfix" id="btm-nav">
      <li><a href="index.php">Home</a></li>
      <li><a href="galeri.php">Galeri</a></li>
      <li><a href="how-to.php">How To</a></li>
      <li><a href="tnc.php">Syarat dan ketentuan</a></li>
      <li class="to-top"><a href="#header">&uarr; Back to top</a></li>
    </ul>
    <ul class="socmed">
      <li><a href="http://twitter.com/" class="tw">Twitter</a></li>
      <li><a href="http://facebook.com/" class="fb">Facebook</a></li>
    </ul><!--social-->
    
    <p id="copyright">&copy; 2014 USBI. All rights reserved.</p>
  </div>
</div><!--footer end-->

</div><!--container-->

<script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</body></html>
