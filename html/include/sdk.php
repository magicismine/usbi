<div class="curtain">
	<div class="inner">
    <div class="page-name">
          <h2 class="modal-title">Syarat & Ketentuan</h2>
        </div>
    
    <div class="scroll-area">
    
    <div class="modal-content">
    	
	    <h4 class="subtitle">TERMS &amp; CONDITIONS A NIGHT OF INFINITE PLEASURE – MAGNUM INFINITY</h4>
	    
	    <ol>
	      <li>Promo ini bebas pungutan. Pajak ditanggung oleh PT. UNILEVER INDONESIA TBK. Hati-hati penipuan,kontak yang dapat dihubungi: Suara Konsumen Unilever di 021-52995299 (pulsa bayar) atau 0-800-1-558000 (bebas pulsa).</li>
	      <li>Kompetisi terbuka untuk semua followers @MyMagnumID.</li>
	      <li>Peserta wajib mengikuti seluruh mekanisme serta syarat dan ketentuan yang telah ditentukan oleh promo Magnum Indonesia yakni, <strong>A Night of Infinite Pleasure</strong>.</li>
	      <li>Peserta adalah WNI dan harus berusia minimal 17 tahun.</li>
	      <li>Kompetisi ini akan berjalan pada:
	      	<ul>
	      		<li>17 Januari 2014 pukul 08.00 – 17.00 WIB</li>
	      		<li>18 Januari 2014 pukul 08.00 – 17.00 WIB</li>
	      		<li>19 Januari 2014 pukul 08.00 – 17.00 WIB</li>
	      		<li>20 Januari 2014 pukul 08.00 – 17.00 WIB</li>
	      		<li>21 Januari 2014 pukul 08.00 – 17.00 WIB</li>
	      		<li>22 Januari 2014 pukul 08.00 – 17.00 WIB</li>
	      	</ul>
	      </li>
	      <li>Pengumuman pemenang A Night of Infinite Pleasure akan berlangsung setiap hari pada pukul 20.00 WIB sesudah promo ditutup pada pukul 17.00 WIB.</li>
	      <li>Kompetisi ini berlangsung di Twitter @MyMagnumID, pertanyaan bisa dilihat di Twitter atau website Magnum.</li>
	      <li>Peserta diminta untuk memberikan alasan kreatif mengapa kalian harus menjadi pemenang untuk diundang ke A Night of  Infinite Pleasure melalui host party yang akan kalian pilih di website Magnum Indonesia http://bit.ly/MagnumHost.</li>
	      <li>Peserta diperbolehkan untuk melakukan tweet dengan alasan sekreatif mungkin, dan juga boleh melakukan tweet lebih dari satu untuk berkesempatan menjadi pemenang.</li>
	      <li>Kalimat kreatif bukan berupa spam, bukan tweet berseri, harus berbeda dari tweet yang sebelumnya, jika ada peserta yang diketahui melakukan hal tersebut maka peserta tersebut akan didiskualifikasi.</li>
	      <li>Peserta hanya berkesempatan menjadi pemenang 1 (satu) kali selama masa promo berlangsung.</li>
	      <li>Penyelenggara akan memilih  15 (lima belas) orang pemenang yg berhak diundang secara eksklusif pada acara A Night of Infinite Pleasure yang berlangsung pada tanggal 24 Januari 2014.</li>
	      <li>Pemenang ditentukan melalui kriteria penjurian.</li>
	      <li>Keputusan juri adalah final dan mengikat serta tidak dapat diganggu gugat.</li>
	      <li>Pemenang akan dipilih oleh panitia #MagnumHost serta Host yang terlibat didalam masa promo ini. Masing-masing pemenang akan mendapatkan dua (2) invitation ke acara A Night of Infinite Pleasure.</li>
	      <li>Juri akan melakukan penilaian berdasarkan, alasan paling kreatif mengenai #MagnumHost ke @MyMagnumID.</li>
	      <li>Pemenang akan mendapatkan Invitation Party ke acara A Night of Infinite, masing-masing pemenang akan mendapatkan 2 (dua) undangan. Total undangan adalah 30 (Tiga puluh).</li>
	      <li>Pemenang Invitatio Party wajib mengikuti syarat dan ketentuan setelah pemilihan pemenang termasuk mematuhi dresscode yang sudah ditentukan pada saat acara A Night of Infinite berlangsung.</li>
	      <li>Jika ditemukan akun twitter peserta dengan nama berbeda, namun pada verifikasi data nama peserta serta alamat menunjukkan satu (1) orang yang sama. Maka kedua peserta tersebut berhak didiskualifikasi oleh panitia penyelenggara promo.</li>
	      <li>Pengambilan Invitation Party akan ditentukan selanjutnya oleh penyelenggara kepada pemenang setelah pengumuman pemenang.</li>	      
	      <li>Pemenang harus mengkonfirmasikan kembali kehadirannya pada saat acara A Night of Infinite berlangsung.</li>
	      <li>Pengumuman pemenang akan diumumkan setiap hari melalui twitter resmi @MyMagnumID dan melalui Host Party yang sudah kalian pilih, serta melalui web resmi Magnum Indonesia di halaman A Night of Infinite.</li>
	      <li>Biaya transportasi dan akomodasi untuk pengambilan Invitation party serta saat Magnum A Night of Infinite diadakan ditanggung oleh masing-masing pemenang.</li>
	      <li>Pemenang wajib memberikan konfimasi mengenai data dirinya untuk sebagai pemenang. Jika dalam waktu 2 x 24 jam dari waktu dihubungi pemenang tidak memberikan konfirmasi maka penyelenggara dapat mengalihkan Invitation party kepada peserta lainnya.</li>
	      <li>Jika dalam jangka waktu yang diinformasikan untuk pengambilan Invitation party tetapi pemenang tidak mengambil Invitation party, maka Invitation party akan hangus. Pemenang tidak berhak untuk menuntut Penyelenggara di kemudian hari.</li>
	      <li>Pemenang harus bisa menunjukkan identitas diri (berupa KTP/SIM/Kartu Pelajar/dll) pada saat pengambilan Invitation party.</li>
	      <li>Keputusan juri/panitia tidak dapat diganggu gugat dan bersifat mutlak.</li>
	      <li>Penyelenggara tidak menerima surat menyurat atau pertanyaan seputar keputusan final.</li>
	      <li>Kompetisi tidak berlaku bagi seluruh karyawan dan keluarganya dari PT. UNILEVER INDONESIA TBK beserta rekanan, agensi dan pihak yang terkait dalam menyelenggarakan promo berupa kompetisi ini.</li>
	      <li>Invitation party tidak dapat diuangkan, dijual maupun dipindahtangankan.</li>
	      <li>Jika karena alasan apapun pemberian Invitation party tidak dapat berjalan sesuai rencana, Magnum memiliki hak untuk memodifikasi ketentuan pemberian Invitation party yang senilai dengan Invitation party yang telah dijanjikan.</li>
	      <li>Dengan mengikuti kompetisi ini peserta setuju untuk mengikuti seluruh ketentuan yang berlaku termasuk tata tertib party jika peserta terpilih sebagai pemenang, termasuk datang tepat waktu, mengenakan dresscode dan menjaga keamanan Magnum A Night of Infinite.</li>
	      <li>Semua hasil data dan hasil karya peserta yang dikirimkan dan diikutsertakan dalam kompetisi ini, serta dokumentasi pemenang pada saat Magnum Infinity Party menjadi hak milik PT Unilever Indonesia, Tbk sepenuhnya dan dapat digunakan, disesuaikan, diubah dan disebarluaskan diperbanyak serta dipublikasikan dalam bentuk apa pun dalam jumlah dan jangka waktu yang tidak terbatas untuk kepentingan Magnum Indonesia.</li>
	      <li>Pihak penyelenggara berhak untuk mengubah dan/atau memodifikasi syarat dan ketentuan promo dari waktu ke waktu dengan atau tanpa pemberitahuan terlebih dahulu.</li>
	      <li>Pihak penyelenggara berhak mendiskualifikasi peserta dan/atau pemenang yang menurut Magnum tidak memenuhi dan/atau melanggar syarat dan ketentuan Promo (termasuk namun tidak terbatas pada isi tweet, verifikasi).</li>	      
	    </ol>
	    
    </div>
    
    </div>
	</div>
</div>