<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie6"><![endif]-->
<!--[if IE 7 ]><html class="ie7"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html id="home" lang="id">
<!--<![endif]-->
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href="css/css.css" type="text/css" rel="stylesheet" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>USBI - Blog Competition</title>
</head>
<body>

<div id="container">

<!--header-->
<div id="header" class="clearfix">
	<div class="container">

    <div id="logo"><a href="index.php"><img src="img/usbi-logo.jpg" alt="USBI - Universitas Siswa Bangsa Internasional"></a></div><!--logo-->

    <ul class="nav" id="top-nav">
      <li class="last"><a href="index.php">Home</a></li>
      <li><a href="galeri.php">Galeri</a></li>
      <li><a href="how-to.php">How To</a></li>
      <li><a href="tnc.php">Syarat dan ketentuan</a></li>
    </ul><!--nav-->

    <ul class="socmed">
      <li><a href="#" class="tw">Twitter</a></li>
      <li><a href="#" class="fb">Facebook</a></li>
    </ul><!--social-->

    <div id="to-nav"><a href="#footer">Nav</a></div><!--to-nav-->

  </div>
</div><!--header end-->

<!--main start-->
<div id="main" class="clearfix">
<div class="container">

<div id="appname">
  <h2 id="name1">Young Future Leader</h2>
  <div id="name2">- Blogging Competition -</div>
</div>

<div id="teaser">
	<div class="wrap">
  	
    <div id="teaser-text">
    	<div class="wrap">
    	<div id="teaser-context">
      	<div>
          <p>Young Future Leader bertujuan menginspirasi generasi muda menjadi pemimpin di masa depan.<br><br> Kini saatnya kamu menulis &ldquo;We are The Future Leaders&rdquo; versimu dan menangkan beasiswa dari USBI.</p>
</div>
			</div>
      
      <div id="cta-home">
    	<a href="registrasi.php" id="cta1">Masukkan Cerita</a>
    	<a href="galeri.php" id="cta2">Lihat Galeri</a>
    </div>
      </div>
    </div>
    
    
    
	  <img src="img/mbp.png" id="mbp" alt=" ">

  </div>
</div>

</div>
</div><!--main end-->

<!--footer-->
<div id="footer" class="clearfix">
  <div class="container">
    <ul class="nav clearfix" id="btm-nav">
      <li><a href="index.php">Home</a></li>
      <li><a href="galeri.php">Galeri</a></li>
      <li><a href="#">How To</a></li>
      <li><a href="#">Syarat dan ketentuan</a></li>
      <li class="to-top"><a href="#header">&uarr; Back to top</a></li>
    </ul>
    <ul class="socmed">
      <li><a href="http://twitter.com/" class="tw">Twitter</a></li>
      <li><a href="http://facebook.com/" class="fb">Facebook</a></li>
    </ul><!--social-->
    
    <p id="copyright">&copy; 2014 USBI. All rights reserved.</p>
  </div>
</div><!--footer end-->

</div><!--container-->

<script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</body></html>
