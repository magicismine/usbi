<?php include('include/header.php'); ?>

<div id="sub-header">
<div class="container">
<div id="appname">
  <h2 id="name1">Young Future Leader</h2>
  <div id="name2">Blogging Competition</div>
</div>
</div>
</div><!--sub-header-->

<!--main start-->
<div id="main" class="clearfix">
<div class="container clearfix">

<h3 class="pagetitle">Kirim tulisan kreatifmu &amp; menangkan beasiswa USBI
</h3>


	<form id="reg-form" class="clearfix" action="success.php">

    <div class="full" id="submission">
      <div class="wrap">
        <label for="url" id="url-label">Masukkan URL tulisan kreatifmu dengan tema<br>"We are The Future Leaders"
</label>
        <input type="text" class="input-text" name="url" id="url" placeholder="Contoh: http://blog.com/nama-artikel">
      </div>
    </div>
    
    <div class="wrap clearfix">

    <div id="task" class="clearfix error">
      <div class="wrap"><!--error message untuk URL address (tampilkan satu persatu)-->Mohon masukkan alamat URL artikel.<br>
<!--error message untuk fields registrasi (tampilkan satu persatu)-->Maaf, mohon masukkan kamu anda dengan lengkap.</div>
    </div>
    
    <div id="reg-instruction">
      <div id="instruction">Agar data kamu tersimpan, isi form registrasi di bawah dengan data asli dan lengkap.
</div>
    </div>

    <p class="part">
      <input type="text" class="input-text" name="nama" id="nama" placeholder="Nama">
    </p> <!--nama-->
    <p class="part">
      <input type="text" class="input-text" name="usia" id="usia" placeholder="Usia" maxlength="8">
    </p> <!--pos-->
    <p class="part">
      <input type="text" class="input-text" name="telepon" id="telepon" placeholder="Telepon" maxlength="18">
    </p> <!--telpon-->
    <p class="part">
      <input type="text" class="input-text" name="email" id="email" placeholder="Email" maxlength="256">
    </p> <!--email-->
    <p id="alamat-wrap">
      <textarea class="textarea" name="alamat" id="alamat" placeholder="Alamat"></textarea>
    </p> <!--alamat-->        
    <p class="clearfix" id="reg-btn-wrap">
      <input type="submit" name="register" id="reg-btn" value="Kirim URL Artikel" class="input-submit button">
    </p>
    <p id="agree-terms" class="clearfix">Dengan melakukan pendaftaran ini, saya menyetujui segala syarat dan ketentuan Blogging Competition ini.</p>
  </div>

  </form>

<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->

<?php include('include/footer.php'); ?>