<?php include('include/header.php'); ?>

<div id="sub-header">
<div class="container">
<div id="cta-top"><a href="submit.php">Submit Cerita</a></div>
<div id="appname">
  <h2 id="name1">Young Future Leader</h2>
  <div id="name2">Blogging Competition</div>
</div>
</div>
</div><!--sub-header-->

<!--main start-->
<div id="main" class="clearfix">
<div class="container clearfix">

<h3 class="pagetitle">Daftar peserta USBI Young Future Leader</h3>
<ul id="post-list" class="clearfix">

  <p class="single-text"><span style="font-size:10px; ">Tulisan peserta yang ditampilkan telah lolos proses moderasi.</span></p>

  <li>
  	<div class="wrap clearfix">
      <a href="#" class="avatar"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
      <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
      <i class="owner">Oleh: <em>Paulo Coelho</em></i>
      <a href="#" class="goto" title="See Article">See Article</a>
    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
      <a href="#" class="avatar"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
      <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
      <i class="owner">Oleh: <em>Paulo Coelho</em></i>
      <a href="#" class="goto" title="See Article">See Article</a>
    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
      <a href="#" class="avatar"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
      <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
      <i class="owner">Oleh: <em>Paulo Coelho</em></i>
      <a href="#" class="goto" title="See Article">See Article</a>
    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
      <a href="#" class="avatar"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
      <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
      <i class="owner">Oleh: <em>Paulo Coelho</em></i>
      <a href="#" class="goto" title="See Article">See Article</a>
    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
      <a href="#" class="avatar"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
      <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
      <i class="owner">Oleh: <em>Paulo Coelho</em></i>
      <a href="#" class="goto" title="See Article">See Article</a>
    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
      <a href="#" class="avatar"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
      <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
      <i class="owner">Oleh: <em>Paulo Coelho</em></i>
      <a href="#" class="goto" title="See Article">See Article</a>
    </div>
  </li>
  <li>
  	<div class="wrap clearfix">
      <a href="#" class="avatar"><img src="img/temp/pic1.jpg" alt="Paulo Coelho"></a>
      <h4 class="title"><a href="#">Masa depan Indonesia ada ditangan pemuda</a></h4>
      <i class="owner">Oleh: <em>Paulo Coelho</em></i>
      <a href="#" class="goto" title="See Article">See Article</a>
    </div>
  </li>
  
</ul>

<ul id="pagination">
	<li><a href="#">&laquo;</a></li>
  <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#">&raquo;</a></li>
</ul>

<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->

<?php include('include/footer.php'); ?>