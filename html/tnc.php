<?php include('include/header.php'); ?>

<div id="sub-header">
<div class="container">
<div id="cta-top"><a href="submit.php">Submit Cerita</a></div>
<div id="appname">
  <h2 id="name1">Young Future Leader</h2>
  <div id="name2">Blogging Competition</div>
</div>
</div>
</div><!--sub-header-->

<!--main start-->
<div id="main" class="clearfix">
<div class="container clearfix">


  
<h3 class="pagetitle">Syarat dan Ketentuan</h3>

<div class="text-wrap clearfix text-only">
	<div class="full">
  <div class="wrap">
  <h4 class="title">1. Persyaratan Umum</h4>
  <ol>
  	<li>Lorem Ipsum. <strong>Proin gravida</strong> nibh vel velit auctor aliquet. <em>Aenean sollicitudin, lorem quis bibendum auctor</em>, nisi elit consequat ipsum.</li>
  	<li>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</li>
  </ol>
  <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
  
  <h4 class="title">2. Persyaratan Khusus</h4>
  <ol>
  	<li>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</li>
  	<li>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</li>
  </ol>
  <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
  </div>
  </div>
</div>

<div class="to-top"><a href="#header">&uarr; Back to top</a></div>

</div>
</div><!--main end-->

<?php include('include/footer.php'); ?>