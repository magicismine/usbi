$(document).ready(function() {

//tab-start		
var tabContainers = $('#data-host > div');
tabContainers.hide().filter(':first').show();
$('#name2 li a').click(function () {
	tabContainers.stop().animate({opacity: '0'},500).hide();
	tabContainers.filter(this.hash).show().stop().animate({opacity: '1'},500);
	$('#name2 li a').removeClass('selected');
	$(this).addClass('selected');
	return false;
}).filter(':first').click()
//tab-end

//show-hide-start
$('#btn-cta a').click(function () {
	$('#hosts').stop(true,false).animate({opacity: '0'},300);
	$('#paper').stop(true,false).animate({height: '198px'},300);
	$('#title1').stop(true,false).animate({opacity: '0'},150, function(){
		$('#title1').css('display','none');
			$('#title2').css('display','inline-block');
			$('#title2').stop(true,false).animate({opacity: '1'},150);
			});
	});

$('#change-host a').click(function () {
	$('#paper').stop(true,false).animate({height: '0'},300);
	$('#hosts').stop(true,false).animate({opacity: '1'},300);
	$('#title2').stop(true,false).animate({opacity: '0'},150, function(){
		$('#title2').css('display','none');
			$('#title1').css('display','inline-block');
			$('#title1').stop(true,false).animate({opacity: '1'},150);
			});
	});
//show-hide-end

/**
* TinyLimiter - scriptiny.com/tinylimiter
* License: GNU GPL v3.0 - scriptiny.com/license
*/
/*(function($) {
	$.fn.extend( {
		limiter: function(limit, elem) {
			$(this).on("keyup focus", function() {
				setCount(this, elem);
			});
			function setCount(src, elem) {
				var chars = src.value.length;
				if (chars > limit) {
					src.value = src.value.substr(0, limit);
					chars = limit;
					//$('#counter').addClass('end');
				}
				elem.html( limit - chars );
			}
			setCount($(this)[0], elem);
		}
	});
})(jQuery);
var elem = $("#chars");
$("#tweet").limiter(140, elem);*/
//TinyLimiter-end

/*	setTimeout(function(){
		
		$('.scroll-area').jScrollPane({showArrows:true, scrollbarWidth: 3, maintainPosition: true});
		
		
		},2000);*/

//modal-start
$('.modal-content').magnificPopup({
	type: 'inline',
	preloader: false,
	removalDelay: 10,
	mainClass: 'mfp-fade'

});
$(document).on('click', '.modal-close', function (e) {
	e.preventDefault();
	$.magnificPopup.close();
});
//modal-end

});


/*$(window).load(function() {
	$('#name2').fadeIn(500, function(){
		//carousel-start
$("#hosts .wrap").jCarouselLite({
		start: 0,
		btnNext: ".next",
		btnPrev: ".prev",
		visible: 3,
		scroll: 3,
		speed: 300,
		mouseWheel: true,
	})
	})*/
//carousel-end




});